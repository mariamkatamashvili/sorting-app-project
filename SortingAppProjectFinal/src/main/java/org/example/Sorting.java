package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Sorting {

    private static final Logger logger = LogManager.getLogger(Sorting.class);

    public static void main(String[] args) {
        logger.info("the program is starting");
        if (args.length > 10) {
            logger.warn("This may throw an exception");
            System.err.print("Only 10 arguments are allowed\n");
            throw new IllegalArgumentException();
        }
        if (args.length == 0) {
            logger.info("Checking if the length equals 0");
            System.out.print("Input is needed");
            return;
        }
        int[] unsortedNumbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                unsortedNumbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException numberFormatException) {
                System.out.print("Inputs should be only integers\n");
                return;
            }
        }

        Arrays.sort(unsortedNumbers);
        logger.info("Printing an array");
        for (int i = 0; i < unsortedNumbers.length; i++) {
            int number = unsortedNumbers[i];
            System.out.print(number);
            if (i != unsortedNumbers.length - 1) {
                System.out.println();
            }
        }
        logger.info("Final message");
    }
}

