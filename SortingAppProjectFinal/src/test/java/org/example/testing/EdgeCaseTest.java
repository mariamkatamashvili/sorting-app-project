package org.example.testing;

import org.example.Sorting;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class EdgeCaseTest {

    private final String[] input;

    public EdgeCaseTest(String[] input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"-3", "7", "12", "-5", "18", "0", "-9", "2", "14", "9", "-150", "-50"}},
                {new String[]{"9", "-3", "7", "12", "-5", "18", "0", "-9", "2", "14", "9", "-150", "-50"}},
                {new String[]{"8", "-2", "9", "5", "-3", "20", "1", "-7", "6", "18", "11", "-180", "-80", "3"}},
                {new String[]{"-4", "6", "11", "-6", "17", "1", "-8", "3", "13", "10", "-100", "-51"}},
                {new String[]{"-2", "9", "5", "-3", "20", "1", "-7", "6", "18", "11", "-180", "-80"}},
                {new String[]{"-6", "10", "4", "-1", "16", "2", "-8", "7", "19", "13", "-150", "-50", "2", "-11", "14", "11", "-210", "-3", "12", "15", "8"}},
                {new String[]{"11", "-3", "12", "6", "-2", "25", "1", "-8", "7", "21", "14", "-190", "-90", "4", "-13"}},
                {new String[]{"-7", "14", "8", "-1", "22", "2", "-9", "10", "24", "17", "-170", "-70", "5", "-14", "18"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptions() {
        Sorting sorting = new Sorting();
        sorting.main(input);
    }
}