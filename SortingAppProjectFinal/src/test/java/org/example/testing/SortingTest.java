package org.example.testing;

import org.example.Sorting;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class SortingTest {

    private ByteArrayOutputStream outputStream;
    private ByteArrayOutputStream errorStream;

    @Before
    public void setUpStreams() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        errorStream = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errorStream));
    }

    @Test
    public void testNoInput() {
        String[] args = {};
        Sorting.main(args);
        String expectedOutput = "Input is needed";
        assertEquals(expectedOutput, outputStream.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenInput() {
        String[] args = {"10", "1", "9", "2", "3", "4", "8", "7", "7", "0", "-100", "34", "2"};
        Sorting.main(args);
        String expectedOutput = "Only 10 arguments are allowed\n";
        assertEquals(expectedOutput, errorStream.toString());
    }

    @Test
    public void testingInvalidInput() {
        String[] args = {"1", "2", "a"};
        Sorting.main(args);
        String expectedOutput = "Inputs should be only integers\n";
        assertEquals(expectedOutput, outputStream.toString());
    }

    @Test
    public void testingSortingWithNormalInputs() {
        String[] args = {"1", "2", "3"};
        String expectedOutput = "the program is starting\n" +
                "the program is starting\n" +
                "Printing an array\n" +
                "Printing an array\n1\n2\n3Final message\n" +
                "Final message\n";
        expectedOutput = expectedOutput.replace("\n", System.lineSeparator());
        Sorting.main(args);
        String actualOutput = outputStream.toString();
        assertEquals(expectedOutput, actualOutput);
    }
}