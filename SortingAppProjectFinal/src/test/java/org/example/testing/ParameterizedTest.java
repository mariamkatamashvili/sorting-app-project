package org.example.testing;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.example.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParameterizedTest {
    private final String[] inputArrayToSort;
    private final String expectedSortedOutput;

    public ParameterizedTest(String[] inputArrayToSort, String expectedSortedOutput) {
        this.inputArrayToSort = inputArrayToSort;
        this.expectedSortedOutput = expectedSortedOutput;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "the program is starting\nthe program is starting\nChecking if the length equals 0\nChecking if the length equals 0\nInput is needed"},
                {new String[]{"1"}, "1"},
                {new String[]{"10", "1", "9", "2", "3", "4", "8", "7", "0", "-100"}, "-100\n0\n1\n2\n3\n4\n7\n8\n9\n10"},
                {new String[0], "Input is needed"}
        });
    }


    @Test
    public void testingWithParameters() {
        String expectedOutput = expectedSortedOutput.replace("\n", System.lineSeparator());
       // String expectedOutput = "the program is starting\n" + "the program is starting\n" + "Checking if the length equals 0\n" + "Checking if the length equals 0\n"  + expectedSortedOutput.replace("\n", System.lineSeparator());
        String actualOutput = getProgramOutput(inputArrayToSort);
        System.out.println("Expected Output:\n" + expectedOutput);
        System.out.println("Actual Output:\n" + actualOutput);
        assertEquals(expectedOutput, actualOutput);
    }



    public String getProgramOutput(String[] input) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream originalOutput = System.out;
        try {
            System.setOut(printStream);
            Sorting.main(input);
            return outputStream.toString();
        } finally {
            System.setOut(originalOutput);
        }
    }
}

